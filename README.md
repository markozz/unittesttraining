# Unit test training #

### Prerequisites ###

* Maven
* Your favorite GIT client
* Your favorite Editor


### Assignment ###

* Verify that the hello greeting contains an explanation mark (!)
* Verify that it greets with goodnight at 03:00
* Verify that it greets with goodday at 13:46
* Verify that it greets with goodmorning at 8:47
* Verify that it greets with goodevening at 21:46
