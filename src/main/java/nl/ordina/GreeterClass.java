package nl.ordina;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GreeterClass {

    public String sayHello(){
        return "Hello!";
    }

    public String sayHelloBasedOnTimeOfDay(Date date) throws ParseException {
        String greeting = "Sorry I don't know how to greet you since I couldn't figure out what time it is..";

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        Date endMorning = formatter.parse("11:00");
        Date endDayTime = formatter.parse("18:00");
        Date endEveningTime = formatter.parse("00:00");
        Date endNightTime = formatter.parse("06:00");

        if(date.before(endNightTime) && date.after(endEveningTime)) {
            greeting = "Goodnight";
        } else if(date.before(endDayTime) && date.after(endMorning)) {
            greeting = "Goodday";
        } else if(date.before(endEveningTime) && date.after(endDayTime)) {
            greeting = "Goodevening";
        } else if(date.before(endMorning) && date.after(endNightTime)) {
            greeting = "Goodmorning";
        }

        return greeting;
    }
}
